# metaROC

This R-package enables meta-analysis of full ROC-curves using various techniques. 

* For the R-code used in the analyses of [A discrete time-to-event model for the meta-analysis of full ROC curves](https://dx.doi.org/10.1002/jrsm.1753), see the branch [discrete_GLMM_paper](https://gitlab.ub.uni-bielefeld.de/stoyef/metaROC/-/tree/discrete_GLMM_paper?ref_type=heads)
* For the R-code used in the analyses of [insert doi here](), see the branch [copulas](https://gitlab.ub.uni-bielefeld.de/stoyef/metaROC/-/tree/copulas?ref_type=heads)

## Features

### Implemented
* simulate data from the following models:
    * discrete GLMMs with categorical variable threshold using either the cloglog- or the logit-link as proposed in Stoye et al. (2024)
    * logit LMM in the specification DIDS as proposed in Steinhauser et al. (2016) 
    * Weibull AFT model with bivariate random effect as proposed in Hoyer et al. (2018)
    * survival copula models with different marginal distributions. Currently available copulas: Clayton copula, asymmetric Joe copula. Currently available marginals: Weibull-binomial, Weibull-normal, loglogistic-binomial, loglogistic-normal, lognormal-binomial, lognormal-normal

* fit the following models to data from several DTA studies reporting results for different diagnostic thresholds:
    * discrete GLMMs with categorical variable threshold using either the cloglog- or the logit-link (Stoye et al., 2024)
    * logit LMM (Steinhauser et al., 2016, DIDS configuration) using a link to the package [`diagmeta`](https://github.com/guido-s/diagmeta)
    * survival copula models with different marginal distributions. Currently available copulas: Clayton copula, asymmetric Joe copula. Currently available marginals: Weibull-binomial, Weibull-normal, loglogistic-binomial, loglogistic-normal, lognormal-binomial, lognormal-normal

## Installation

You can install this package branch using the following code in your R console:

`devtools::install_git("https://gitlab.ub.uni-bielefeld.de/stoyef/metaROC", build_vignettes = T)`

To build the vignette, you need to have the package `rmarkdown` installed. Omit `build_vignettes = T` to avoid building the vignette when installing the package.

## Literature

* Stoye FV, Tschammler C, Kuss O, Hoyer A. A discrete time‐to‐event model for the meta‐analysis of full ROC curves. Research synthesis methods. 2024. http://dx.doi.org/10.1002/jrsm.1753
* Hoyer A, Hirt S, Kuss O. Meta-analysis of full ROC curves using bivariate time-to-event models for interval-censored data. Research synthesis methods. 2018;9:62-72. http://dx.doi.org/10.1002/jrsm.1273
* Steinhauser S, Schumacher M, Rücker G. Modelling multiple thresholds in meta-analysis of diagnostic test accuracy studies. BMC Medical Research Methodology. 2016;16:97. http://dx.doi.org/10.1186/S12874-016-0196-1
